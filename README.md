**TOYS RENT - KELOMPOK D13**
============================

Anggota Kelompok
================
- Angginistanti Fairuz Hanun (1706984524) 
- Farhan Azyumardhi Azmi (1706979234) 
- Janitra Ariena Sekarputri (1706979316) 
- Steffi Alexandra (1706043992) 

Developer Section
=================
Agar project dapat dijalankan di local, lakukan hal-hal berikut:
- Buat file `.env` di direktori yang sama dengan `manage.py`.
- Buat variabel-variabel berikut pada `.env`:
  - DB_NAME=\
    Isi dengan nama database yang dibuat di local.
  - DB_USER=\
    Isi dengan nama user yang dibuat pada PostgreSQL local.
  - DB_PASS=\
    Isi dengan password yang terhubung dengan user di atas.
  - SECRET_KEY=\
    Isi dengan secret key untuk Django, bisa didapat di sini: https://www.miniwebtool.com/django-secret-key-generator/