from django import forms

from utils.db_query_utils import do_raw_sql


class CreateBarangForm(forms.Form):
    id_barang = forms.CharField(label="ID Barang", max_length=10, required=True)
    nama_item = forms.ChoiceField(label="Nama Item",
                                  choices=[
                                      (item["nama"], item["nama"])
                                      for item in do_raw_sql("SELECT nama FROM item", fetch=True)
                                  ],
                                  required=True)
    warna = forms.CharField(label="Warna", max_length=50, required=False, initial="")
    url_foto = forms.URLField(label="URL Foto", required=False, initial="")
    kondisi = forms.CharField(label="Kondisi", required=True, initial="")
    lama_penggunaan = forms.IntegerField(label="Lama Penggunaan", min_value=0, required=False, initial=0)
    owner = forms.ChoiceField(label="Pemilik/Penyewa",
                              choices=[
                                  (member["nama_lengkap"], member["nama_lengkap"])
                                  for member in do_raw_sql("SELECT nama_lengkap FROM pengguna "
                                                           "WHERE no_ktp IN "
                                                           "(SELECT no_ktp FROM anggota)", fetch=True)
                              ],
                              required=True)

    def __init__(self, *args, **kwargs):
        super(CreateBarangForm, self).__init__(*args, **kwargs)
        levels_list = do_raw_sql("SELECT nama_level FROM level_keanggotaan", fetch=True)
        for level in levels_list:
            royalti_field_name = "persen royalti %s" % (level["nama_level"])
            harga_sewa_field_name = "harga sewa %s" % (level["nama_level"])
            self.fields[royalti_field_name] = forms.IntegerField(label="Persen Royalti %s" % (level["nama_level"]),
                                                                 min_value=0, initial=0)
            self.fields[harga_sewa_field_name] = forms.IntegerField(label="Harga Sewa %s" % (level["nama_level"]),
                                                                    min_value=0, initial=0)

    def clean(self):
        cleaned_data = super(CreateBarangForm, self).clean()
        id_barang = cleaned_data.get("id_barang")
        fetched_barang_list = do_raw_sql("SELECT id_barang FROM barang", fetch=True)
        for barang in fetched_barang_list:
            if id_barang == barang["id_barang"]:
                raise forms.ValidationError("ID barang yang dimasukkan sudah pernah terdaftar!")
        return cleaned_data

    def get_royalti_fields(self):
        for field_name in self.fields:
            if field_name.startswith("persen royalti"):
                yield self[field_name]

    def get_harga_sewa_fields(self):
        for field_name in self.fields:
            if field_name.startswith("harga sewa"):
                yield self[field_name]


class UpdateBarangForm(CreateBarangForm):

    def __init__(self, *args, **kwargs):
        id_barang = kwargs.pop("id_barang", None)
        super(UpdateBarangForm, self).__init__(*args, **kwargs)
        barang_object = do_raw_sql("SELECT * FROM barang WHERE id_barang='{}'".format(id_barang),
                                   fetch=True)[0]
        info_barang_level_objects = do_raw_sql("SELECT * FROM info_barang_level WHERE id_barang='{}'"
                                               .format(id_barang), fetch=True)
        self.fields["id_barang"].initial = barang_object["id_barang"]
        self.fields["id_barang"].disabled = True
        self.fields["nama_item"].initial = barang_object["nama_item"]
        self.fields["warna"].initial = barang_object["warna"]
        self.fields["url_foto"].initial = barang_object["url_foto"]
        self.fields["kondisi"].initial = barang_object["kondisi"]
        self.fields["lama_penggunaan"].initial = barang_object["lama_penggunaan"]
        owner_name = do_raw_sql("SELECT nama_lengkap FROM pengguna WHERE no_ktp='{}'"
                                .format(barang_object["no_ktp_penyewa"]), fetch=True)[0]
        self.fields["owner"].initial = owner_name["nama_lengkap"]
        for level in info_barang_level_objects:
            for royalti_fields in self.get_royalti_fields():
                if royalti_fields.label.endswith(level["nama_level"]):
                    royalti_fields.initial = level["porsi_royalti"]
            for harga_sewa_fields in self.get_harga_sewa_fields():
                if harga_sewa_fields.label.endswith(level["nama_level"]):
                    harga_sewa_fields.initial = level["harga_sewa"]

    def clean(self):
        return super(CreateBarangForm, self).clean()
