from django.urls import path
from . import views


urlpatterns = [
    path('', views.list_barang, name='list-barang'),
    path('fetch-barang', views.get_list_barang, name='fetch-list-barang'),
    path('add', views.add_barang, name='add-barang'),
    path('<str:id_barang>/update', views.update_barang, name='update-barang'),
    path('<str:id_barang>', views.detail_barang, name='detail-barang'),
    path('<str:id_barang>/delete', views.delete_barang, name='delete-barang')
]
