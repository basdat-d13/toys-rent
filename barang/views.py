from django.http import JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from utils.db_query_utils import do_raw_sql
from .forms import CreateBarangForm, UpdateBarangForm


def detail_barang(request, id_barang):
    if "no_ktp" in request.session.keys():
        barang_object = do_raw_sql("SELECT * FROM barang WHERE id_barang='{}'".format(id_barang),
                                   fetch=True)
        related_reviews = do_raw_sql("SELECT tanggal_review, review FROM barang_dikirim WHERE id_barang='{}'"
                                     .format(id_barang), fetch=True)
        return render(request, 'barang_detail.html', context={"barang": barang_object[0], "reviews": related_reviews})
    else:
        return HttpResponseRedirect(reverse("login"))


def get_list_barang(request):
    if "no_ktp" in request.session.keys():
        barang_objects = do_raw_sql("SELECT id_barang, nama_item, warna, kondisi FROM barang",
                                    fetch=True)
        return JsonResponse({"data": barang_objects})
    else:
        return HttpResponseRedirect(reverse("login"))


def list_barang(request):
    if "no_ktp" in request.session.keys():
        return render(request, 'barang_list.html')
    else:
        return HttpResponseRedirect(reverse("login"))


def add_barang(request):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        level_barang = do_raw_sql("SELECT nama_level FROM level_keanggotaan", fetch=True)
        if request.method == "POST":
            form = CreateBarangForm(request.POST)

            if form.is_valid():
                id_barang = form.cleaned_data["id_barang"]
                owner_name = form.cleaned_data["owner"]
                owner_object = do_raw_sql("SELECT no_ktp FROM pengguna WHERE nama_lengkap='{}'".format(owner_name),
                                          fetch=True)
                do_raw_sql("INSERT INTO barang VALUES ('{}', '{}', '{}', '{}', '{}', {}, '{}')".format(
                    id_barang, form.cleaned_data["nama_item"], form.cleaned_data["warna"],
                    form.cleaned_data["url_foto"], form.cleaned_data["kondisi"], form.cleaned_data["lama_penggunaan"],
                    owner_object[0]["no_ktp"]
                ))

                info_barang_data = [(field_data, form.cleaned_data[field_data])
                                    for field_data in form.cleaned_data.keys()
                                    if field_data.startswith("persen royalti") or field_data.startswith("harga sewa")]
                for i in range(len(info_barang_data) // 2):
                    nama_level = info_barang_data[i * 2][0].split(" ")[-1]
                    persen_royalti = info_barang_data[i * 2][-1]
                    harga_sewa = info_barang_data[(i * 2) + 1][-1]
                    do_raw_sql("INSERT INTO info_barang_level VALUES ('{}', '{}', {}, {})"
                               .format(id_barang, nama_level, harga_sewa, persen_royalti))

                return HttpResponseRedirect(reverse("list-barang"))

        else:
            form = CreateBarangForm()
        return render(request, "barang_tambah.html", {"form": form, "level_barang": level_barang})
    else:
        return HttpResponseRedirect(reverse("login"))


def update_barang(request, id_barang):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        level_barang = do_raw_sql("SELECT nama_level FROM level_keanggotaan", fetch=True)
        if request.method == "POST":
            kwargs = {"id_barang": id_barang}
            form = UpdateBarangForm(data=request.POST, **kwargs)

            if form.is_valid():
                owner_name = form.cleaned_data["owner"]
                owner_object = do_raw_sql("SELECT no_ktp FROM pengguna WHERE nama_lengkap='{}'".format(owner_name),
                                          fetch=True)
                do_raw_sql(
                    "UPDATE barang SET nama_item='{}', warna='{}', url_foto='{}', kondisi='{}', "
                    "lama_penggunaan='{}', no_ktp_penyewa='{}' WHERE id_barang='{}'".format(
                        form.cleaned_data["nama_item"], form.cleaned_data["warna"], form.cleaned_data["url_foto"],
                        form.cleaned_data["kondisi"], form.cleaned_data["lama_penggunaan"], owner_object[0]["no_ktp"],
                        id_barang
                    ))

                info_barang_data = [(field_data, form.cleaned_data[field_data])
                                    for field_data in form.cleaned_data.keys()
                                    if field_data.startswith("persen royalti") or field_data.startswith("harga sewa")]
                for i in range(len(info_barang_data) // 2):
                    nama_level = info_barang_data[i * 2][0].split(" ")[-1]
                    persen_royalti = info_barang_data[i * 2][-1]
                    harga_sewa = info_barang_data[(i * 2) + 1][-1]
                    existing_info_barang_level = do_raw_sql("SELECT nama_level FROM info_barang_level "
                                                            "where id_barang='{}'".format(id_barang),
                                                            fetch=True)
                    if nama_level in [item["nama_level"] for item in existing_info_barang_level]:
                        do_raw_sql(
                            "UPDATE info_barang_level SET porsi_royalti={}, harga_sewa={} "
                            "WHERE id_barang='{}' AND nama_level='{}'".format(
                                persen_royalti, harga_sewa, id_barang, nama_level)
                        )
                    else:
                        do_raw_sql("INSERT INTO info_barang_level VALUES ('{}', '{}', {}, {})"
                                   .format(id_barang, nama_level, harga_sewa, persen_royalti))

                return HttpResponseRedirect(reverse("list-barang"))
        else:
            kwargs = {"id_barang": id_barang}
            form = UpdateBarangForm(**kwargs)

        return render(request, "barang_update.html", {"form": form, "level_barang": level_barang, "id": id_barang})
    else:
        return HttpResponseRedirect(reverse("login"))


def delete_barang(request, id_barang):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        do_raw_sql("DELETE from barang WHERE id_barang='{}'".format(id_barang))
        return HttpResponseRedirect(reverse("list-barang"))
    else:
        return HttpResponseRedirect(reverse("login"))
