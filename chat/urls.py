from django.urls import path
from . import views

urlpatterns = [
    path('admin', views.chat_admin_page, name='chat-admin-page'),
    path('member', views.chat_anggota_page, name='chat-member-page'),
    path('send-chat', views.send_chat, name='send-chat'),
    path('fetch-chats', views.fetch_chats, name='fetch-chats'),
]
