import random
import string
import time

from django.http import HttpResponseNotAllowed, JsonResponse, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from utils.db_query_utils import do_raw_sql


def chat_admin_page(request):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        user_objects = do_raw_sql("SELECT pengguna.no_ktp, nama_lengkap FROM pengguna, anggota "
                                  "WHERE anggota.no_ktp=pengguna.no_ktp"
                                  .format(request.session["no_ktp"]), fetch=True)
        return render(request, 'chat_admin.html', context={"users": user_objects})
    else:
        return HttpResponseRedirect(reverse("login"))


def chat_anggota_page(request):
    if "no_ktp" in request.session.keys() and request.session["type"] == "anggota":
        admin_objects = do_raw_sql("SELECT pengguna.nama_lengkap, pengguna.no_ktp "
                                   "FROM pengguna, admin WHERE admin.no_ktp=pengguna.no_ktp", fetch=True)
        randomly_selected_admin = random.choice(admin_objects)
        return render(request, 'chat_anggota.html', context={"admin": randomly_selected_admin})
    else:
        return HttpResponseRedirect(reverse("login"))


def send_chat(request):
    if "no_ktp" in request.session.keys():
        if request.method == "POST":
            chat_id = "".join(random.choice(string.ascii_letters) for i in range(15))
            message = request.POST["pesan"]
            current_datetime = time.strftime("%Y-%m-%d %H:%M:%S")
            member_ktp = request.POST["no_ktp_anggota"]
            admin_ktp = request.POST["no_ktp_admin"]
            do_raw_sql("INSERT into chat VALUES ('{}', '{}', '{}', '{}', '{}')"
                       .format(chat_id, message, current_datetime, member_ktp, admin_ktp))
            new_chat_object = do_raw_sql("SELECT pesan, date_time, no_ktp_anggota, no_ktp_admin "
                                         "FROM chat WHERE pesan='{}' AND date_time ='{}' AND "
                                         "no_ktp_anggota='{}' AND no_ktp_admin='{}'"
                                         .format(message, current_datetime, member_ktp, admin_ktp), fetch=True)
            return JsonResponse({"new_chat": new_chat_object[0]})
        else:
            return HttpResponseNotAllowed(["POST"], "Request method '{}' allowed!".format(request.method))
    else:
        return HttpResponseRedirect(reverse("login"))


def fetch_chats(request):
    if "no_ktp" in request.session.keys():
        if request.method == "GET":
            member_ktp = request.GET["no_ktp_anggota"]
            admin_ktp = request.GET["no_ktp_admin"]
            chat_objects = do_raw_sql("SELECT pesan, date_time, no_ktp_anggota, no_ktp_admin "
                                      "FROM chat WHERE no_ktp_anggota='{}' AND no_ktp_admin='{}'"
                                      "ORDER BY date_time"
                                      .format(member_ktp, admin_ktp), fetch=True)
            return JsonResponse({"chats": chat_objects})
        else:
            return HttpResponseNotAllowed(["GET"], "Request method '{}' allowed!".format(request.method))
    else:
        return HttpResponseRedirect(reverse("login"))
