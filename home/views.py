from django.shortcuts import render


# Create your views here.

def index(request):
    request.session['is_login'] = True
    request.session['role'] = 'admin'
    return render(request, 'home.html')
