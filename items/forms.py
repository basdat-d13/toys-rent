from django import forms
from utils.db_query_utils import do_raw_sql

class CreateItem(forms.Form):
    nama_barang = forms.CharField(label="Nama Barang", max_length=255, required=True, widget=forms.TextInput(attrs={'id' : 'nama_barang' , 'placeholder': 'Nama Barang'}))
    deskripsi = forms.CharField(label = "Deskripsi", required=True, widget=forms.TextInput(attrs={'id' : 'deskripsi' , 'placeholder': 'Deskripsi Barang'}))
    usia_awal = forms.IntegerField(min_value=0,initial=0, required = True, label="Usia Dari", widget=forms.NumberInput(
                attrs={'id' : 'usia_awal', }))
    usia_akhir = forms.IntegerField(label="Usia Sampai", min_value=0, required=True, initial=0,widget=forms.NumberInput(attrs={'id' : 'usia_akhir'}))
    bahan =forms.CharField(label = "Bahan", required=True, widget=forms.TextInput(attrs={'id' : 'bahan' , 'placeholder': 'Bahan'}))
    kategori = forms.ChoiceField(label="Kategori",
                                  choices=[
                                      (item["nama"], item["nama"])
                                      for item in do_raw_sql("SELECT nama FROM kategori", fetch=True)
                                  ],
                                  required=True, widget=forms.Select(attrs={'id':'kategori'}))

class UpdateItem(CreateItem):
    def __init__(self, *args, **kwargs):
        nama_barang = kwargs.pop("nama_barang", None)
        super(CreateItem, self).__init__(*args, **kwargs)
        items = do_raw_sql("SELECT * FROM item WHERE nama ='{}'".format(nama_barang),
                                   fetch=True)[0]
        self.fields["nama_barang"].initial = items['nama']
        self.fields["nama_barang"].disabled = True
        self.fields["deskripsi"].initial = items['deskripsi']
        self.fields["usia_awal"].initial = items['usia_dari']
        self.fields["usia_akhir"].initial = items['usia_sampai']
        self.fields["bahan"].initial = items["bahan"]
        """ 
        kategori = do_raw_sql("SELECT nama_kategori FROM kategori_item WHERE nama_item ='{}'"
                                .format(items["nama"]), fetch=True)[0]
        self.fields["kategori"].initial = kategori["nama_kategori"] """
    def clean(self):
        return super(CreateItem, self).clean()


