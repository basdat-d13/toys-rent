from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('create_item', views.create_item, name='create_item'),
    path('daftar_item', views.daftar_item, name='daftar_item'),
    path('update_item', views.update_item, name='update_item'),
    path('show_item_list', views.show_item_list, name='show_item_list'),
    path('add_item', views.add_item, name="add_item"),
    path('delete_item/<var>', views.delete_item, name="delete_item"),
    path('update', views.update, name="update"),
]
