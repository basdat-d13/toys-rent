from django.shortcuts import render
from .forms import CreateItem, UpdateItem
from django.http import JsonResponse
from utils.db_query_utils import do_raw_sql
from django.core.exceptions import ValidationError
from django.http import HttpResponseRedirect
from django.urls import reverse

# Create your views here.
def create_item(request):
    forms = CreateItem()
    return render(request,'create_item.html', {'forms' : forms})

def daftar_item(request):
    response = {}
    return render(request,'daftar_item_admin.html',response)

def update_item(request):
    forms = UpdateItem()
    return render(request,'update_item.html', {'forms' : forms})

def update(request):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        if request.method == "POST":
            kwargs = {"nama_barang": nama_barang}
            form = UpdateItem(data=request.POST, **kwargs)
            kategori = form.cleaned_data["kategori"]
            owner_object = do_raw_sql("SELECT nama FROM kategori WHERE nama ='{}'".format(kategori),
                                    fetch=True)
            do_raw_sql(
                "UPDATE item SET nama ='{}', deskripsi='{}', usia_dari='{}', usia_sampai='{}', "
                "bahan='{}', kategori='{}' WHERE nama='{}'".format(
                    form.cleaned_data["nama_barang"], form.cleaned_data["deskripsi"], form.cleaned_data["usia_awal"],
                    form.cleaned_data["usia_akhir"], form.cleaned_data["bahan"], owner_object[0]["kategori"],
                    nama_barang
                ))

            return HttpResponseRedirect(reverse("daftar-item"))
        else:
            kwargs = {"nama_barang": nama_barang}
            form = UpdateItem(**kwargs)

        return render(request, "update_item.html", {"form": form, "nama": nama_barang})
    else:
        return HttpResponseRedirect(reverse("login"))


def delete_item(request, var):
    if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
        do_raw_sql("DELETE from item WHERE nama='{}'".format(var))
        do_raw_sql("DELETE from kategori_item where nama_item='{}'".format(var))
        return HttpResponseRedirect(reverse("daftar_item"))
    else:
        return HttpResponseRedirect(reverse("login"))

def show_item_list(request):
    item_list = do_raw_sql("SELECT nama, deskripsi, usia_dari, usia_sampai, bahan, nama_kategori from ITEM, KATEGORI_ITEM where KATEGORI_ITEM.nama_item = ITEM.nama", fetch=True)
    try:
        return JsonResponse({'item_list' : item_list, 'type' : request.session['type']})
    except AttributeError:
        return JsonResponse({'item_list' : item_list})

def add_item(request):
    form = CreateItem()
    if request.method == 'POST':
        nama_barang = request.POST['nama_barang']
        usia_awal = request.POST['usia_awal']
        usia_akhir = request.POST['usia_akhir']
        deskripsi = request.POST['deskripsi']
        bahan = request.POST['bahan']
        kategori = request.POST['kategori']
        result = do_raw_sql("SELECT nama from ITEM WHERE nama = '{}'".format(nama_barang), fetch = True)
        if len(result) > 0 :
                raise ValidationError("The item with the same name already exists!")
        else :
                do_raw_sql("INSERT INTO item VALUES ('{}', '{}', '{}', '{}', '{}')".format(nama_barang, deskripsi, usia_awal, usia_akhir, bahan))
                do_raw_sql("INSERT INTO kategori_item VALUES('{}', '{}')".format(nama_barang, kategori))
                return HttpResponseRedirect(reverse('daftar_item'))
