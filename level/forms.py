from django import forms

from utils.db_query_utils import do_raw_sql


class FormCreateLevel(forms.Form):
	nama_level = forms.CharField(label="Nama Level", max_length=20, required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'size': '40'}))
	minimum_poin = forms.IntegerField(label="Minimum Poin",required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'size': '40'}))
	deskripsi = forms.CharField(label="Deskripsi", max_length=50, required=False, widget=forms.TextInput(attrs={'class': 'form-control', 'size': '40'}))

	def __init__(self, *args, **kwargs):
		super(FormCreateLevel, self).__init__(*args, **kwargs)
		
	class Meta:
		fields = ['nama_level', 'minimum_poin', 'deskripsi']

		
class FormUpdateLevel(FormCreateLevel):
	def __init__(self, *args, **kwargs):
		nama_level= kwargs.pop("nama_level", None)
		super(FormUpdateLevel, self).__init__(*args, **kwargs)
		level_obj = do_raw_sql("SELECT * FROM level_keanggotaan WHERE nama_level='{}'".format(nama_level),fetch=True)[0]
		self.fields["nama_level"].initial = level_obj["nama_level"]
		self.fields["minimum_poin"].initial = level_obj["minimum_poin"]
		self.fields["deskripsi"].initial = level_obj["deskripsi"]
		
	class Meta:
		fields = ['nama_level', 'minimum_poin', 'deskripsi']
