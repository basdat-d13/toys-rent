from django.urls import path
from . import views

urlpatterns = [
    path('', views.daftar_level, name='daftar_level'),
    path('<str:nama_level>/update_level', views.update_level, name='update_level'),
	path('create_level', views.create_level, name='create_level'),
	path('<str:nama_level>/delete_level', views.delete_level, name='delete_level'),
]
