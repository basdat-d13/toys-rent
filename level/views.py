from django.shortcuts import render
from django.http import HttpResponse

from utils.db_query_utils import do_raw_sql
from django.http import HttpResponseRedirect
from .forms import FormCreateLevel, FormUpdateLevel

response = {}
# Create your views here.
def daftar_level (request):
	if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
		level_obj = do_raw_sql("SELECT * FROM level_keanggotaan ORDER BY minimum_poin ASC",fetch=True)
		return render (request, 'daftar_level.html',{"level": level_obj})
	else:
		return HttpResponseRedirect('/')
   
def update_level (request, nama_level):
	kwargs = {"nama_level": nama_level}
	form = FormUpdateLevel(data=request.POST, **kwargs)
	if request.method == "POST":   
		print("request valid!")
		if form.is_valid():
			print("form valid!")
			do_raw_sql(
				"UPDATE level_keanggotaan SET nama_level='{}', minimum_poin='{}', deskripsi='{}' WHERE nama_level='{}'".format(
					form.cleaned_data["nama_level"], form.cleaned_data["minimum_poin"], form.cleaned_data["deskripsi"],
					nama_level
				))
			return HttpResponseRedirect('/level/')
	else:
		kwargs = {"nama_level": nama_level}
		form = FormUpdateLevel(**kwargs)
	return render(request, 'update_level.html', {"form": form})

	

def create_level (request):
	if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
		form = FormCreateLevel(request.POST)
		if request.method == "POST":
			if form.is_valid():
				response['nama_level'] = request.POST['nama_level']
				response['minimum_poin'] = request.POST['minimum_poin']
				response['deskripsi'] = request.POST['deskripsi']
				do_raw_sql("INSERT INTO level_keanggotaan VALUES ('{}', '{}', '{}')".format( response['nama_level'],response['minimum_poin'],response['deskripsi']))
				return HttpResponseRedirect('/level/')
		else:
			form = FormCreateLevel()
		return render(request, 'create_level.html', {"form": form})
	else:
		return HttpResponseRedirect('/')

def delete_level(request, nama_level):
	do_raw_sql("DELETE from level_keanggotaan WHERE nama_level='{}'".format(nama_level))
	return HttpResponseRedirect('/level/')
