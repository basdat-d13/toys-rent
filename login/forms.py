from django import forms
from .models import LoginModel, RegisterModel
from django.core.validators import validate_email

class LoginForm(forms.Form):
    no_ktp = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={'id' : 'no_ktp' , 'placeholder': 'No. KTP'}))
    email = forms.EmailField(max_length=50, required=True, widget=forms.EmailInput(attrs={'id' : 'email' , 'placeholder': 'E-mail', 'class' : 'form-control'}))

class RegisterForm(forms.Form):
    attrs = {
        'class' : 'form-control',
        'placeholder': 'E-mail',
        'id' : 'email' ,
    }

    no_ktp = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={'id' : 'no_ktp' , 'placeholder': 'No. KTP'}))
    nama_lengkap = forms.CharField(max_length=255, required=True, widget=forms.TextInput(attrs={'id' : 'name' , 'placeholder': 'Nama Lengkap'}))
    email = forms.EmailField(max_length=50, required=True, widget=forms.EmailInput(attrs=attrs))
    date = forms.DateTimeField(required=True, widget=forms.TextInput(attrs={'id' : 'bdate' ,'placeholder': 'Birthdate'}))
    no_telp = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={'id' : 'no_telp' , 'placeholder': 'No. Telp'}))
    alamat_nama = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'id' : 'alamat_nama' , 'placeholder': 'Nama'}))
    alamat_jalan = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'id' : 'alamat_jalan' , 'placeholder': 'Jalan'}))
    alamat_no = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'id' : 'alamat_no' , 'placeholder': 'No'}))
    alamat_kota = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'id' : 'alamat_kota' , 'placeholder': 'Kota'}))
    alamat_kodepos = forms.CharField(max_length=100, required=True, widget=forms.TextInput(attrs={'id' : 'alamat_kodepos' , 'placeholder': 'Kode Pos'}))

class AdminRegisterForm(forms.Form):
    attrs = {
        'class' : 'form-control',
        'placeholder': 'E-mail',
        'id' : 'email' ,
    }

    no_ktp = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={'id' : 'no_ktp' , 'placeholder': 'No. KTP'}))
    nama_lengkap = forms.CharField(max_length=255, required=True, widget=forms.TextInput(attrs={'id' : 'name' , 'placeholder': 'Nama Lengkap'}))
    email = forms.EmailField(max_length=50, required=True, widget=forms.EmailInput(attrs=attrs))
    date = forms.DateTimeField(required=True, widget=forms.TextInput(attrs={'id' : 'bdate' ,'placeholder': 'Birthdate'}))
    no_telp = forms.CharField(max_length=20, required=True, widget=forms.TextInput(attrs={'id' : 'no_telp' , 'placeholder': 'No. Telp'}))
