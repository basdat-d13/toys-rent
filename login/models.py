from django.db import models

# Create your models here.

class LoginModel(models.Model):
    no_ktp = models.CharField(max_length=20)
    email = models.EmailField(max_length=50)

class RegisterModel(models.Model):
    no_ktp = models.CharField(max_length=20)
    nama_lengkap = models.CharField(max_length=255)
    email = models.EmailField(max_length=50)
    date = models.DateField()
    no_telp = models.CharField(max_length=20)
    alamat = models.TextField(max_length=100)


