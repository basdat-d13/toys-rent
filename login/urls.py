from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('login', views.login_page, name='login'),
    path('register_anggota', views.register_anggota, name='register_anggota'),
    path('register_admin', views.register_admin, name='register_admin'),
    path('register_anggota_page', views.register_anggota_page, name='register_anggota_page'),
    path('register_admin_page', views.register_admin_page, name='register_admin_page'),
    path('post_login', views.login, name='post_login'),
    path('logout', views.logout, name='logout'),
    path('choice', views.choice, name="choice"),
]
