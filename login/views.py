from django.shortcuts import render
from .forms import LoginForm, RegisterForm, AdminRegisterForm
from django.http import HttpResponseRedirect
from .models import LoginModel, RegisterModel
from django.core.exceptions import ValidationError
from utils.db_query_utils import do_raw_sql
from django.urls import reverse
from django.core.validators import validate_email
from django.http import JsonResponse
import time
import datetime
# import pg

# Create your views here.

def login_page(request):
        forms = LoginForm()
        return render(request,'login.html', {'forms':forms})

def register_anggota_page(request):
        forms = RegisterForm()
        return render(request,'register_anggota.html', {'forms':forms})

def register_admin_page(request):
        forms = AdminRegisterForm()
        return render(request,'register_admin.html', {'forms':forms})

def choice(request):
        return render(request, 'choice.html')

def login(request):
        if request.method == 'POST':
                try:
                        ktp = request.POST['no_ktp']
                        mail = request.POST['email']
                        result = do_raw_sql("SELECT no_ktp, email from PENGGUNA WHERE no_ktp = '{}' AND email= '{}'".format(ktp, mail), fetch = True)
                        if len(result) == 0 :
                                raise ValidationError("The user does not exist!")
                        else :
                                check_auth = do_raw_sql("SELECT no_ktp FROM ADMIN WHERE no_ktp = '{}'".format(ktp), fetch=True)
                                if len(check_auth) == 0 :
                                        no_ktp = request.POST['no_ktp']
                                        email = request.POST['email']
                                        request.session['no_ktp'] = no_ktp
                                        request.session['email'] = email
                                        request.session['type'] = 'anggota'
                                        
                                        return HttpResponseRedirect(reverse('home:index'))
                                else :
                                        no_ktp = request.POST['no_ktp']
                                        email = request.POST['email']
                                        request.session['no_ktp'] = no_ktp
                                        request.session['email'] = email
                                        request.session['type'] = 'admin'
                                        
                                        return HttpResponseRedirect(reverse('home:index'))
                except ValueError:
                        return JsonResponse({"status": "1"})

def register_anggota(request): 
        if request.method == 'POST':
                ktp = request.POST.get('no_ktp')
                mail = request.POST.get('email')
                no_telp = request.POST.get('no_telp')
                name = request.POST.get('name')
                bdate = request.POST.get('bdate')
                alamat_nama = request.POST.get('alamat_nama')
                alamat_jalan = request.POST.get('alamat_jalan')
                alamat_no = request.POST.get('alamat_no')
                alamat_kota = request.POST.get('alamat_kota')
                alamat_kodepos = request.POST.get('alamat_kodepos')
                result = do_raw_sql("SELECT no_ktp, email from PENGGUNA WHERE no_ktp = '{}' AND email= '{}'".format(ktp, mail), fetch = True)
                if len(result) > 0 :
                        raise ValidationError("The user already exists!")
                else :
                        do_raw_sql("INSERT INTO pengguna VALUES ('{}', '{}', '{}', '{}', '{}')".format(ktp, name, mail, bdate, no_telp))
                        do_raw_sql("INSERT INTO anggota VALUES('{}', '{}', '{}')".format(ktp, 0, 'bronze'))
                        do_raw_sql("INSERT INTO alamat VALUES('{}', '{}', '{}', '{}', '{}', '{}')".format(ktp, alamat_nama, alamat_jalan, alamat_no, alamat_kota, alamat_kodepos))
                        return HttpResponseRedirect(reverse('home:index'))

def register_admin(request): 
        if request.method == 'POST':
                ktp = request.POST.get('no_ktp')
                mail = request.POST.get('email')
                no_telp = request.POST.get('no_telp')
                name = request.POST.get('name')
                bdate = request.POST.get('bdate')
                result = do_raw_sql("SELECT no_ktp, email from PENGGUNA WHERE no_ktp = '{}' AND email= '{}'".format(ktp, mail), fetch = True)
                if len(result) > 0 :
                        raise ValidationError("The user already exists!")
                else :
                        do_raw_sql("INSERT INTO pengguna VALUES ('{}', '{}', '{}', '{}', '{}')".format(ktp, name, mail, bdate, no_telp))
                        do_raw_sql("INSERT INTO admin VALUES ('{}')".format(ktp))
                        return HttpResponseRedirect(reverse('home:index'))

def logout(request):
    request.session.flush()
    return HttpResponseRedirect(reverse('home:index'))

def checkUser(request):
        if 'no_ktp' not in request.session :
                return JsonResponse({"status": "1"})
        else :
                return JsonResponse({'no_ktp' : no_ktp, 'email' : email})