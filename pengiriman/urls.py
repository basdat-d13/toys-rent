from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('create_pengiriman', views.create_pengiriman, name='create_pengiriman'),    
    path('daftar_pengiriman/<str:page>', views.daftar_pengiriman, name='daftar_pengiriman'),
    path('update_pengiriman', views.update_pengiriman, name='update_pengiriman'),
    path('delete_pengiriman', views.delete_pengiriman, name='delete_pengiriman'),
    path('post_create_pengiriman', views.post_create_pengiriman, name='post_pengiriman'),
]
