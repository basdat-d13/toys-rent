from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
def create_pengiriman(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("select b.nama_item from barang b, barang_pesanan p where p.id_barang = b.id_barang")
    barang = cursor.fetchall()
    hasil1 = list(dict.fromkeys(barang))
    print(hasil1)
    response['barang_pesanan'] = hasil1

    cursor.execute("select nama from alamat")
    alamat = cursor.fetchall()
    hasil2 = list(dict.fromkeys(alamat))
    print(hasil2)
    response['alamat'] = hasil2
    cursor.close()
    return render(request,'create_pengiriman.html',response)

def post_create_pengiriman(request):
    if(request.method=="POST"):
        list_barang = request.POST.getlist('barang_pesanan')
        list_alamat = request.POST.getlist('alamat')
        tanggal = request.POST['tanggal']
        metode = request.POST['metode']
        ongkos = request.POST['ongkos']
        id_pemesanan = request.POST['id_pemesanan']
        no_resi = request.POST['no_resi']
        no_ktp_anggota = request.POST['no_ktp_anggota']
        
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM PENGIRIMAN WHERE id_pemesanan='"+id_pemesanan+"' AND no_resi='"+no_resi+";")
        hasil = cursor.fetchone()
        if(hasil):
            if len(hasil) > 0:
                return HttpResponseRedirect('/pengiriman/create_pengiriman/')
                print("a")
        cursor.execute("INSERT INTO PENGIRIMAN(no_resi,id_pemesanan,metode,ongkos) values("+"'"+no_resi+"','" +id_pemesanan+"','"+metode+"',"+ongkos+",'"+tanggal+"','"+no_ktp_anggota+"',"+list_alamat+"');")
        cursor.close()
        # for i in list_alamat:
        #     cursor.execute("INSERT INTO ALAMAT(no_ktp_anggota,nama) values("+"'"+no_ktp_anggota+"','"+i+"');")
        #     print("b")

        # for i in list_barang:
        #     cursor.execute("INSERT INTO BARANG_PESANAN(id_pemesanan,nama) values("+"'"+no_ktp_anggota+"','"+i+"');")
        #     print("b")

        return HttpResponseRedirect('/pengiriman/daftar_pengiriman/1')
    else:
        print("c")
        return HttpResponseRedirect('/pengiriman/create_pengiriman/1')

def daftar_pengiriman(request, page=None):
    response = {}
    cursor = connection.cursor()
    cursor.execute('SELECT p.no_resi, tanggal, b.id_barang, p.id_pemesanan, ((lama_sewa*harga_sewa)+ongkos) as total FROM PENGIRIMAN p, BARANG_PESANAN b , INFO_BARANG_LEVEL i WHERE p.id_pemesanan = b.id_pemesanan AND i.id_barang = b.id_barang')
    list_pengiriman = cursor.fetchall()
    data_pengiriman = [{'id_pengiriman':i[0],
        'tanggal' : i[1],
        'daftar_barang': i[2],
        'id_pemesanan': i[3],
        'harga_total': i[4]}
    for i in list_pengiriman]
    page = request.GET.get('page', 1)
    paginator = Paginator(data_pengiriman, 10)
    try:
        data = paginator.page(page)
    except PageNotAnInteger:
        data = paginator.page(1)
    except EmptyPage:
        data = paginator.page(paginator.num_pages)
    response['list_pengiriman'] = data
    cursor.close()
    return render(request,'daftar_pengiriman.html',response)

@csrf_exempt
def update_pengiriman(request):
    if(request.method=="POST"):
            list_barang = request.POST.getlist('barang_pesanan')
            list_alamat = request.POST.getlist('alamat')
            tanggal = request.POST['tanggal']
            metode = request.POST['metode']
            ongkos = request.POST['ongkos']
            id_pemesanan = request.POST['id_pemesanan']
            no_resi = request.POST['no_resi']
            no_ktp_anggota = request.POST['no_ktp_anggota']
            
            cursor = connection.cursor()
            cursor.execute("UPDATE PENGIRIMAN SET barang_pesanan='"+list_barang+"',alamat='"+list_alamat+"',tanggal='"+tanggal+"',metode='"+metode+"'where id_pemesanan='"+id_pemesanan+"';") 
            cursor.close()   
    return HttpResponseRedirect('/pengiriman/daftar_pengiriman/1')

@csrf_exempt
def delete_pengiriman(request):
    if(request.method=="POST"):
        id_pemesanan = request.POST['id']
        cursor = connection.cursor()
        cursor.execute("DELETE FROM PENGIRIMAN WHERE id_pemesanan='"+id_pemesanan+"';")
        cursor.execute("DELETE FROM BARANG_PESANAN WHERE id_pemesanan='"+id_pemesanan+"';")
        cursor.close()
        return HttpResponseRedirect('/pengiriman/daftar_pengiriman/1')
    else:
        return HttpResponseRedirect('/pengiriman/daftar_pengiriman/1')

