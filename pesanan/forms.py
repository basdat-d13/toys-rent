from django import forms

from utils.db_query_utils import do_raw_sql

			   
class FormCreatePesanan(forms.Form):
	anggota = forms.ChoiceField(label="Anggota",
                              choices=[(member["no_ktp"], member["no_ktp"])
                                  for member in do_raw_sql("SELECT no_ktp FROM ANGGOTA ", fetch=True)],required=True,  widget=forms.Select(attrs={'class': 'form-control'}))
	nama_barang = forms.MultipleChoiceField(label="Barang",
                              choices=[(member["nama_item"], member["nama_item"])
                                  for member in do_raw_sql("SELECT nama_item FROM barang ", fetch=True)],widget=forms.SelectMultiple(attrs={'class': 'form-control'}),required=True)
	lama_sewa = forms.IntegerField(label="Lama Sewa",required=True, widget=forms.TextInput(attrs={'class': 'form-control', 'size': '40'}))

	def __init__(self, *args, **kwargs):
		super(FormCreatePesanan, self).__init__(*args, **kwargs)
		
	class Meta:
		fields = ['anggota', 'nama_barang', 'lama_sewa']
		
class FormUpdatePesanan(FormCreatePesanan):
	def __init__(self, *args, **kwargs):
		nama_level= kwargs.pop("nama_level", None)
		super(FormUpdatePesanan, self).__init__(*args, **kwargs)
		level_obj = do_raw_sql("SELECT * FROM level_keanggotaan WHERE nama_level='{}'".format(nama_level),fetch=True)[0]
		self.fields["nama_level"].initial = level_obj["nama_level"]
		self.fields["minimum_poin"].initial = level_obj["minimum_poin"]
		self.fields["deskripsi"].initial = level_obj["deskripsi"]
		
	class Meta:
		fields = ['nama_level', 'minimum_poin', 'deskripsi']