from django.urls import path
from . import views

urlpatterns = [
    path('', views.daftar_pesanan, name='daftar_pesanan'),
	path('update_pesanan', views.update_pesanan, name='update_pesanan'),
	path('create_pesanan', views.create_pesanan, name='create_pesanan'),
	path('<str:id_pemesanan>/delete_pesanan', views.delete_pesanan, name='delete_pesanan'),
]
