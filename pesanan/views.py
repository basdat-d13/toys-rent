from django.shortcuts import render
from django.http import HttpResponse

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.http import HttpResponseRedirect

from .forms import FormCreatePesanan

from utils.db_query_utils import do_raw_sql

# Create your views here.
def daftar_pesanan (request):
	if "no_ktp" in request.session.keys() and request.session["type"] == "admin":
		pesanan_obj = do_raw_sql("SELECT P.id_pemesanan, nama_item, BP.status FROM PEMESANAN P RIGHT OUTER JOIN BARANG_PESANAN BP ON BP.id_pemesanan=P.id_pemesanan NATURAL JOIN BARANG ORDER BY P.id_pemesanan ASC",fetch=True)
		id_pesanan_obj = do_raw_sql("SELECT id_pemesanan, harga_sewa, status FROM PEMESANAN ORDER BY id_pemesanan ASC",fetch=True)
		page = request.GET.get('page', 1)
		data = Paginator(id_pesanan_obj, 10)
		try:
			users = data.page(page)
		except PageNotAnInteger:
			users = data.page(1)
		except EmptyPage:
			users = data.page(paginator.num_pages)
		return render(request, 'daftar_pesanan.html', {'id': id_pesanan_obj, 'pesan':pesanan_obj, 'data':users })
	else:
		pesanan_obj = do_raw_sql("SELECT P.id_pemesanan, nama_item, BP.status FROM PEMESANAN P RIGHT OUTER JOIN BARANG_PESANAN BP ON BP.id_pemesanan=P.id_pemesanan NATURAL JOIN BARANG WHERE no_ktp_pemesan='rPu22C0X' ORDER BY P.id_pemesanan ASC",fetch=True)
		id_pesanan_obj = do_raw_sql("SELECT id_pemesanan, harga_sewa, status FROM PEMESANAN WHERE no_ktp_pemesan='rPu22C0X' ORDER BY id_pemesanan ASC",fetch=True)
		page = request.GET.get('page', 1)
		data = Paginator(id_pesanan_obj, 10)
		try:
			users = data.page(page)
		except PageNotAnInteger:
			users = data.page(1)
		except EmptyPage:
			users = data.page(paginator.num_pages)
		return render(request, 'daftar_pesanan_anggota.html', {'id': id_pesanan_obj, 'pesan':pesanan_obj, 'data':users })
		
   
def create_pesanan (request):
	form = FormCreatePesanan(request.POST)
	if request.method == "POST":
		if form.is_valid():
			response['anggota'] = request.POST['anggota']
			response['nama_barang'] = request.POST['nama_barang']
			response['lama_sewa'] = request.POST['lama_sewa']
			return HttpResponseRedirect('/pesanan/')
	else:
		form = FormCreatePesanan()
	return render(request, 'create_pesanan.html', {"form": form})
   
def update_pesanan (request):
	return render (request, 'update_pesanan.html')  

def delete_pesanan(request, id_pemesanan):
	do_raw_sql("DELETE from pemesanan WHERE id_pemesanan='{}'".format(id_pemesanan))
	return HttpResponseRedirect('/pesanan/')
	
   