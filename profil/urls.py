from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('profil', views.profil, name='profil'),
    path('profile_display', views.profile_display, name="profile_display"),
]
