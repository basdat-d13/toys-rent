from django.shortcuts import render
from django.http import JsonResponse
from utils.db_query_utils import do_raw_sql

# Create your views here.

def profil(request):
    response = {}
    return render(request,'profil.html',response)

def profile_display(request):
        result = do_raw_sql("SELECT no_ktp, nama_lengkap, email, tanggal_lahir, no_telp FROM pengguna WHERE no_ktp = '{}'".format(request.session['no_ktp']), fetch=True)
        check_auth = do_raw_sql("SELECT no_ktp FROM ADMIN WHERE no_ktp = '{}'".format(request.session['no_ktp']), fetch=True)
        anggota = do_raw_sql("SELECT poin FROM ANGGOTA WHERE no_ktp = '{}'".format(request.session['no_ktp']), fetch=True)
        user_address = do_raw_sql("SELECT nama, jalan, nomor, kota, kodepos FROM ALAMAT WHERE no_ktp_anggota = '{}'".format(request.session['no_ktp']), fetch=True)
        if len(check_auth) == 0 :
            return JsonResponse({'no_ktp' : result[0]['no_ktp'], 'nama_lengkap' : result[0]['nama_lengkap'], 'email' : result[0]['email'], 'tanggal_lahir': result[0]["tanggal_lahir"], 'no_telp' : result[0]["no_telp"], 'alamat' : user_address, 'type' : 'Pengguna', 'poin' : anggota})
        else :
            return JsonResponse({'no_ktp' : result[0]['no_ktp'], 'nama_lengkap' : result[0]['nama_lengkap'], 'email' : result[0]['email'], 'tanggal_lahir': result[0]["tanggal_lahir"], 'no_telp' : result[0]["no_telp"], 'alamat' : user_address, 'type' : 'Admin'})
