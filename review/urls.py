from django.contrib import admin
from django.urls import path
from . import views

urlpatterns = [
    path('create_review', views.create_review, name='create_review'),    
    path('daftar_review', views.daftar_review, name='daftar_review'),
    path('update_review', views.update_review, name='update_review'),
    path('delete_review', views.delete_review, name='delete_review'),
    path('post_create_review', views.post_create_review, name='post_create_review'),
]
