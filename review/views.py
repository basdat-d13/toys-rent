from django.shortcuts import render
from django.db import connection
from django.http import HttpResponseRedirect,HttpResponse,JsonResponse
from django.urls import reverse
from django.contrib import messages
from django.views.decorators.csrf import csrf_exempt

# Create your views here.
def create_review(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute("select b.nama_item from barang b, barang_pesanan p where p.id_barang = b.id_barang")
    barang = cursor.fetchall()
    hasil1 = list(dict.fromkeys(barang))
    cursor.close()
    print(hasil1)
    response['barang_pesanan'] = hasil1
    return render(request,'create_review.html',response)

def post_create_review(request):
    if(request.method=="POST"):
        review = request.POST['review']
    
        cursor = connection.cursor()
        cursor.execute("INSERT INTO BARANG_DIKIRIM(review) values("+"','" +review+");")
        return HttpResponseRedirect('/review/daftar_review/')
    else:
        print("c")
        return HttpResponseRedirect('/review/create_review/')


def daftar_review(request):
    response = {}
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM BARANG_DIKIRIM')
    review = cursor.fetchall()
    cursor.close()
    response['review'] = review
    return render(request,'daftar_review.html',response)

def update_review(request):
    response = {}
    return render(request,'update_review.html',response)

@csrf_exempt
def delete_review(request):
    if(request.method=="POST"):
        no_resi = request.POST['no_resi']
        id_barang = request.POST['id_barang']
        no_urut = request.POST('no_urut')
        cursor = connection.cursor()
        cursor.execute("SELECT no_urut FROM BARANG_DIKIRIM WHERE no_resi ='"+no_resi+" and id_barang='"+id_barang+";")
        no_urut = cursor.fetchall()
        cursor.execute("DELETE FROM BARANG_DIKIRIM WHERE no_resi ='"+no_resi+" and no_urut='"+no_urut+";")
        cursor.close()
        print("a")
        return HttpResponseRedirect('/review/daftar_review/')
    else:
        print("b")
        return HttpResponseRedirect('/review/daftar_review/')

