"""toys_rent URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from home.views import index

urlpatterns = [
    path('', index, name='home'),
    path('admin/', admin.site.urls),
    path('home/', include(('home.urls', 'home'), namespace="home")),
    path('pengiriman/', include('pengiriman.urls')),
    path('items/', include('items.urls')),
    path('profil/', include('profil.urls')),
    path('login/', include('login.urls')),
    path('pesanan/', include('pesanan.urls')),
    path('level/', include('level.urls')),
    path('review/', include('review.urls')),
    path('chat/', include('chat.urls')),
    path('barang/', include('barang.urls')),
]

